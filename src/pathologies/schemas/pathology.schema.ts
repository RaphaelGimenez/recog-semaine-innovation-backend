import { Prop, Schema, SchemaFactory } from "@nestjs/mongoose"
import { Document } from "mongoose"

@Schema()
export class Pathology extends Document {
  @Prop({ required: true })
  name: string

  @Prop({ default: false })
  valid: boolean
}

export const PathologySchema = SchemaFactory.createForClass(Pathology)
