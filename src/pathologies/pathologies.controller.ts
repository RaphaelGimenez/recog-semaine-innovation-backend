import {
  Controller,
  Get,
  Post,
  Body,
  Query,
  Delete,
  Param,
} from "@nestjs/common"
import { PathologyQueryDto } from "./dto/pathology-query.dto"
import { Pathology } from "./schemas/pathology.schema"
import { PathologiesService } from "./pathologies.service"
import { CreatePathologyDto } from "./dto/create-pathology.dto"

@Controller("pathologies")
export class PathologiesController {
  constructor(private readonly pathologiesService: PathologiesService) {}

  @Get()
  findAll(@Query() pathologyQueryDto: PathologyQueryDto): Promise<Pathology[]> {
    return this.pathologiesService.findByQuery(pathologyQueryDto)
  }

  @Post()
  createOne(
    @Body() createPathologyDto: CreatePathologyDto,
  ): Promise<Pathology> {
    return this.pathologiesService.create(createPathologyDto)
  }

  @Delete(":id")
  async deleteById(@Param("id") id: string): Promise<{ status: string }> {
    return this.pathologiesService.deleteById(id)
  }
}
