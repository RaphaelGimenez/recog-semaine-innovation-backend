import { Module } from "@nestjs/common"
import { MongooseModule } from "@nestjs/mongoose"
import { Pathology, PathologySchema } from "./schemas/pathology.schema"
import { PathologiesController } from "./pathologies.controller"
import { PathologiesService } from "./pathologies.service"

@Module({
  imports: [
    MongooseModule.forFeature([
      { name: Pathology.name, schema: PathologySchema },
    ]),
  ],
  controllers: [PathologiesController],
  providers: [PathologiesService],
})
export class PathologiesModule {}
