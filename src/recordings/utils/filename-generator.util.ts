import { Request } from "express"
import { nanoid } from "nanoid"
import { File } from "../interfaces/file.interface"

export const fileNameGenerator = (
  req: Request,
  file: File,
  callback: (arg, name) => void,
): void => {
  const timestamp = new Date().getTime()
  const id = nanoid(6)
  callback(null, `${timestamp}_${id}`)
}
