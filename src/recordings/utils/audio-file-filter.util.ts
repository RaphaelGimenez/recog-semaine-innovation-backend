import { Request } from "express"
import { HttpException, HttpStatus } from "@nestjs/common"
import { File } from "../interfaces/file.interface"

export const audioFileFilter = (
  req: Request,
  file: File,
  callback: (arg, name) => void,
): void => {
  const { mimetype } = file

  if (mimetype !== "audio/mpeg") {
    callback(
      new HttpException("Only audio files are accepted", HttpStatus.FORBIDDEN),
      false,
    )
  }

  callback(null, true)
}
