export class SearchQueryDto {
  startDate: number

  endDate: number

  pathology: string

  order: "desc" | "asc"

  ids: string
}
