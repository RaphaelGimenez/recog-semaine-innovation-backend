import { File } from "../interfaces/file.interface"

export class CreateRecordingDto {
  uid: string

  files: File[]

  pathology: string

  date: number
}
