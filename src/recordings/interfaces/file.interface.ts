export interface File {
  fieldname: string
  filename: string
  originalname: string
  encoding: string
  mimetype: string
  buffer: Buffer
  path: string
  destination: string
  size: number
}
