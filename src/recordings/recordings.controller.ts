import {
  Controller,
  Get,
  Post,
  Body,
  UseInterceptors,
  UploadedFiles,
  Param,
  Query,
  Delete,
  Logger,
} from "@nestjs/common"
import { FilesInterceptor } from "@nestjs/platform-express"
import { diskStorage } from "multer"
import { SearchQueryDto } from "./dto/search-query.dto"
import { audioFileFilter } from "./utils/audio-file-filter.util"
import { Recording } from "./schemas/recording.schema"
import { destinationGenerator } from "./utils/destination-generator.util"
import { File } from "./interfaces/file.interface"
import { fileNameGenerator } from "./utils/filename-generator.util"
import { CreateRecordingDto } from "./dto/create-recording.dto"
import { RecordingsService } from "./recordings.service"

@Controller("recordings")
export class RecordingsController {
  constructor(private readonly recordingsService: RecordingsService) {}

  @Get()
  findAll(@Query() searchQueryDto: SearchQueryDto): Promise<Recording[]> {
    return this.recordingsService.findByQuery(searchQueryDto)
  }

  @Get(":uid")
  findAllForUser(@Param("uid") uid: string): Promise<Recording[]> {
    return this.recordingsService.findByUid(uid)
  }

  @Delete(":id")
  deleteById(@Param("id") id: string): Promise<{ status: string }> {
    return this.recordingsService.deleteById(id)
  }

  @Post()
  @UseInterceptors(
    FilesInterceptor("files", 3, {
      storage: diskStorage({
        destination: destinationGenerator,
        filename: fileNameGenerator,
      }),
      fileFilter: audioFileFilter,
    }),
  )
  async create(
    @Body() createRecordingDto: CreateRecordingDto,
    @UploadedFiles() files: File[],
  ): Promise<Recording> {
    try {
      return await this.recordingsService.create(createRecordingDto, files)
    } catch (error) {
      Logger.error(new Error(error))
      throw new Error(error)
    }
  }
}
