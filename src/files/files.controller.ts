import { Response } from "express"
import {
  Controller,
  Get,
  Res,
  Param,
  Query,
  HttpException,
  HttpStatus,
} from "@nestjs/common"
import { FilesService } from "./files.service"
import { SearchQueryDto } from "../recordings/dto/search-query.dto"

@Controller("files")
export class FilesController {
  constructor(private readonly filesService: FilesService) {}

  @Get("download")
  async downloadRecordsArchive(
    @Res() res: Response,
    @Query() searchQueryDto: SearchQueryDto,
  ): Promise<void> {
    try {
      const zipPath = await this.filesService.createArchive(searchQueryDto)
      return res.download(zipPath)
    } catch (error) {
      throw new HttpException(
        {
          status: HttpStatus.FORBIDDEN,
          error: "An error occured while creating the archive",
        },
        HttpStatus.FORBIDDEN,
      )
    }
  }

  @Get("uploads/:uid/:name")
  getRecordingFile(
    @Param("uid") uid: string,
    @Param("name") name: string,
    @Res() res: Response,
  ): void {
    res.sendFile(name, { root: `uploads/${uid}` })
  }
}
