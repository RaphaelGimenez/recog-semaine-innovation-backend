import { Module } from "@nestjs/common"
import { RecordingsModule } from "../recordings/recordings.module"
import { FilesController } from "./files.controller"
import { FilesService } from "./files.service"

@Module({
  providers: [FilesService],
  controllers: [FilesController],
  imports: [RecordingsModule],
})
export class FilesModule {}
