# Recog API

## Description

This API use the [Nest](https://github.com/nestjs/nest) framework.

## Installation

```bash
$ yarn install
```

## Running the app

```bash
# development
$ yarn start:nest

# watch mode
$ yarn start:dev

# production mode
$ yarn start
```

## Test

```bash
# unit tests
$ yarn test

# e2e tests
$ yarn test:e2e

# test coverage
$ yarn test:cov
```

## Server config and deployment

### Description

This app is meant to get deployed with Dokku. You can easly create a server with Dokku preinstalled using Digital Ocean One click apps.

### Deployment

```bash
# HOST

$ dokku apps:create <app-name>

# LOCAL

$ cd <app-name>

$ git remote add dokku dokku@<hostname_or_ip>:<app-name>

$ git push dokku master
```

### Domain config

Create your wanted subdomain and add a new A record pointing to the server ipv4

### HTTPS Config

```bash
$ sudo dokku plugin:install https://github.com/dokku/dokku-letsencrypt.git

$ sudo dokku plugin:update letsencrypt

$ dokku config:set --no-restart <app-name> DOKKU_LETSENCRYPT_EMAIL=<your_email>

$ dokku letsencrypt <app-name>
```
